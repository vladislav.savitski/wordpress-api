<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class Api_Wp_Admin {

    private $option_name = 'api_opt';
    private $plugin_name;

    private $version;

    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function add_options_page() {

        $this->plugin_screen_hook_suffix = add_options_page(
            __( 'Api', 'api-wp' ),
            __( 'Api Settings', 'api-wp' ),
            'manage_options',
            $this->plugin_name,
            array( $this, 'display_options_page' )
        );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        wp_enqueue_script(
            $this->plugin_name . '-api-elem',
            plugin_dir_url(__FILE__) . 'js/api-block.js',
            array('wp-blocks','wp-editor', 'wp-element', 'wp-data', 'jquery'),
            true
        );
        wp_enqueue_script(
            $this->plugin_name . '-api-handler',
            plugin_dir_url(__FILE__) . 'js/api-handler.js',
            array('wp-blocks','wp-editor', 'wp-element', 'wp-data', 'jquery'),
            true
        );

    }

    public function display_options_page() {
        include_once 'partials/api-wp-admin-display.php';
    }

    public function register_setting() {
        add_settings_section(
            $this->option_name . '_general',
            __( 'API settings', 'api-wp' ),
            array( $this, $this->option_name . '_general_cb' ),
            $this->plugin_name
        );
        add_settings_field(
            $this->option_name . '_apikey',
            __( 'API Key', 'api-wp' ),
            array( $this, $this->option_name . '_apikey_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_apikey' )
        );
        add_settings_field(
            $this->option_name . '_partner_key',
            __( 'Partner Key', 'api-wp' ),
            array( $this, $this->option_name . '_partner_key_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_partner_key' )
        );
        add_settings_field(
            $this->option_name . '_url',
            __( 'URL', 'api-wp' ),
            array( $this, $this->option_name . '_url_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_url' )
        );
        add_settings_field(
            $this->option_name . '_url_sys',
            __( 'URL', 'api-wp' ),
            array( $this, $this->option_name . '_url_sys_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_url_sys' )
        );

        register_setting( $this->plugin_name, $this->option_name . '_apikey');
        register_setting( $this->plugin_name, $this->option_name . '_partner_key');
        register_setting( $this->plugin_name, $this->option_name . '_url');
        register_setting( $this->plugin_name, $this->option_name . '_url_sys');

    }

    public function api_opt_apikey_cb() {
        $apikey = get_option( $this->option_name . '_apikey' );
        echo '<input type="text" name="' . $this->option_name . '_apikey' . '" id="' . $this->option_name . '_apikey' . '" value="' . $apikey . '"> ' . __( 'Insert the ApplicationToken. ', 'api-wp' );
    }

    public function api_opt_partner_key_cb() {
        $partner_key = get_option( $this->option_name . '_partner_key' );
        echo '<input type="text" name="' . $this->option_name . '_partner_key' . '" id="' . $this->option_name . '_partner_key' . '" value="' . $partner_key . '"> ' . __( 'Insert the Partner Token. ', 'api-wp' );
    }

    public function api_opt_url_cb() {
        $url = get_option( $this->option_name . '_url' );
        echo '<input type="text" name="' . $this->option_name . '_url' . '" id="' . $this->option_name . '_url' . '" value="' . $url . '"> ' . __( 'URL. ', 'api-wp' );
    }

    public function api_opt_url_sys_cb() {
        $url = get_option( $this->option_name . '_url_sys' );
        echo '<input type="text" name="' . $this->option_name . '_url_sys' . '" id="' . $this->option_name . '_url_sys' . '" value="' . $url . '"> ' . __( 'URL sys. ', 'api-wp' );
    }

    public function api_opt_general_cb() {
        echo '<p>' . __( 'To use this plugin, you must have a valid API key.', 'api-wp' ) . '</p>';
    }
}
