/* This section of the code registers a new block, sets an icon and a category, and indicates what type of fields it'll include. */

wp.blocks.registerBlockType('api/api-form', {
    title: 'API block',
    icon: 'smiley',
    category: 'common',
    attributes: {
        shortcode: {
            type: 'string',
            source: 'text'
        },
    },
    edit: function(props) {
        return React.createElement(
            "div",
            null,
            React.createElement(
                "h5",
                null,
                "API BLOCK"
            ),
        );
    },
    save: function(props) {
        return wp.element.createElement(
            "div",
            { style: {  } },
            "[apitag]"
        );
    }
});
