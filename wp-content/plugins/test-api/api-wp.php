<?php

/**
 * Test API to Wordpress
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.1
 * @package           API_wp
 *
 * @wordpress-plugin
 * Plugin Name:       Test API
 * Description:       Test API
 * Version:           0.0.1
 * Author:            Vladislav
 * Text Domain:       api-test
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

require plugin_dir_path( __FILE__ ) . 'includes/class-api-wp.php';




/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_api_wp() {
    $plugin = new Api_Wp();
    $plugin->run();

}
run_api_wp();
add_action('init', 'start_session', 1);
function start_session() {
    if(!session_id()) {
        session_start();
    }
}