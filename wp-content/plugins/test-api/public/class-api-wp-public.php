<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.eventilla.com
 * @since      1.0.0
 *
 * @package    Eventilla_Wp
 * @subpackage Eventilla_Wp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Eventilla_Wp
 * @subpackage Eventilla_Wp/public
 * @author     Eventilla <tuki@eventilla.com>
 */
class Api_Wp_Public {

    /**
     * The plugin options
     *
     * @since 1.0.0
     * @access private
     * @var string $options The plugin options.
     */
    private $options;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name       The name of the plugin.
     * @param      string $version    The version of this plugin.
     */
    public function __construct( $plugin_name = 'api-wp', $version = '1.0.0' ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->set_options();

    }

    public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/api-wp-public.css', array(), $this->version, 'all' );
        $prefix = 'eventilla_opt';
        $css_editor = '_css_editor';
        $css_code = get_option( $prefix . $css_editor , '' );
        wp_add_inline_style( $this->plugin_name, $css_code );
    }

    public function enqueue_scripts() {
        wp_enqueue_script('api-handler', plugin_dir_url(__FILE__) . 'js/api-handler.js', array( 'jquery' ), $this->version, false);
        wp_localize_script( 'api-handler', 'apiajax' , array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce( 'event_login' ) ) );
        wp_localize_script( 'api-handler', 'apiajax' , array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce( 'event_reset' ) ) );
    }

    private function set_options() {
        $this->options = get_option( $this->plugin_name . '-options' );
    }

    public function event_login() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->login($_POST['User_PhoneNumber'], $_POST['password']);
        echo $response;
        wp_die();
    }

    public function event_set() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->set($_POST['User_PhoneNumber'], $_POST['User_Gender'], $_POST['User_FirstName'], $_POST['User_LastName'], $_POST['User_Address'], $_POST['User_City'], $_POST['User_ZipCode'], $_POST['User_EMail'], $_POST['User_BirthDay']);
        echo $response;
        wp_die();
    }

    public function event_reset() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->reset($_POST['User_PhoneNumber']);
        echo $response;
        wp_die();
    }

    public function event_get() {
        $api_request = new Api_Wp_Request();
        if (empty($_SESSION['User_PhoneNumber'])) {
            $_SESSION['User_PhoneNumber'] = $_POST['User_PhoneNumber'];
            $response = $api_request->get($_POST['User_PhoneNumber']);
            $response->User_PhoneNumber =  $_POST['User_PhoneNumber'];
        } elseif(!empty($_SESSION['User_PhoneNumber'])){
            $response = $api_request->get($_SESSION['User_PhoneNumber']);
            $response->User_PhoneNumber =  $_SESSION['User_PhoneNumber'];
        }
//        if (is_string($response)) {
//            $_SESSION['User_PhoneNumber'] = null;
//        }
        echo json_encode($response);
        wp_die();
    }

    public function event_pin_set() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->pin_set($_POST['User_PhoneNumber'], $_POST['User_OldPinCode'], $_POST['User_NewPinCode']);
        echo $response;
        wp_die();
    }

    public function event_notification() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->notification($_POST['User_PhoneNumber'], $_POST['User_OffersByEMail'], $_POST['User_OffersBySMS']);
        echo $response;
        wp_die();
    }

    public function event_delete() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->delete($_POST['User_PhoneNumber'], $_POST['User_PinCode']);
        echo $response;
        wp_die();
    }

    public function event_session() {
        session_destroy();
    }

    public function get_session() {
        if (empty($_SESSION['User_PhoneNumber'])) {
            echo "false";
            wp_die();
        } elseif(!empty($_SESSION['User_PhoneNumber'])){
            echo "true";
            wp_die();
        }
    }

    public function event_register() {
        $api_request = new Api_Wp_Request();
        $response = $api_request->register($_POST['User_PhoneNumber'], $_POST['User_Gender'], $_POST['User_FirstName'], $_POST['User_LastName'], $_POST['User_Address'], $_POST['User_City'], $_POST['User_ZipCode'], $_POST['User_EMail'], $_POST['User_BirthDay']);
        echo $response;
        wp_die();
    }
}
