(function( $ ) {
    'use strict';

    $(function() {
        $('#login-form').submit(function(e) {
            $(".preloader").show();
            e.preventDefault();
            var login = $('#login').val();
            var password = $('#password').val();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_PhoneNumber': login,
                    'password': password,
                    'action': 'event_login'
                },
                success:function(data) {
                    console.log(data);
                    if(data == 0) {
                        $.ajax({
                            url: apiajax.ajax_url,
                            type: 'POST',
                            data: {
                                'User_PhoneNumber': login,
                                'action': 'event_get'
                            },
                            success: function (data) {
                                // console.log(data);
                                var dataObject = JSON.parse(data);
                                window.User_PhoneNumber = dataObject.User_PhoneNumber;
                                $('#user_tel').html($('#user_tel').text() + '<strong>' + dataObject.User_PhoneNumber + '</strong>');
                                dataDisplay(dataObject);
                                $('.login-container').css('display', 'none');
                                $('.dashboard-container').css('display', 'flex');

                                // $('#tagErrorMsgLogin').css('display', 'block');
                                // $('#tagErrorMsgLogin').html('<div><p>' + data + '</p></div>');

                            },
                            error: function (errorThrown) {
                                console.log(errorThrown);
                            }
                        });
                    }
                    else {
                        $('#tagErrorMsgLogin').css('display', 'block');
                        $('#tagErrorMsgLogin').html('<div><p>' + data + '</p></div>');
                    }
                    $(".preloader").hide();

                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#last-form-register').submit(function (e){
            $(".preloader").show();
            e.preventDefault();
            var user_gender = $('input[name="radio"]:checked').val();
            var first_name = $('#first-name').val();
            var last_name = $('#last-name').val();
            var phone_number = $('#phone-number').val();
            var user_address = $('#user-address').val();
            var user_zipcode = $('#user-zipcode').val();
            var user_city = $('#user-city').val();
            var user_email = $('#mail').val();
            var user_birthday = $('#anniv').val();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_Gender': user_gender,
                    'User_FirstName': first_name,
                    'User_LastName': last_name,
                    'User_PhoneNumber': phone_number,
                    'User_Address': user_address,
                    'User_ZipCode': user_zipcode,
                    'User_City': user_city,
                    'User_EMail': user_email,
                    'User_BirthDay': user_birthday,
                    'action': 'event_register'
                },
                success:function(data) {
                    $(".preloader").hide();
                    if (data == 0){
                        $('#errorRegister').css('display', 'none');
                        $('.block-customer-congrats').css('display', 'block');
                        $('.back-to-login').css('display', 'none');
                    } else {
                        $('#errorRegister').css('display', 'block');
                        $('#errorRegister').html('<div><p>' + data + '</p></div>');
                    }
                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#return-register-form').click(function (e) {
            e.preventDefault();
            location.reload();
        });
        $('#modifier-last-form').submit(function (e) {
            e.preventDefault();
            $(".preloader").show();
            var user_gender = $('input[name="radio-modifier"]:checked').val();
            var first_name = $('#modifier-first-name').val();
            var last_name = $('#modifier-last-name').val();
            var phone_number = window.User_PhoneNumber;
            var user_address = $('#modifier-user-address').val();
            var user_zipcode = $('#modifier-user-zipcode').val();
            var user_city = $('#modifier-user-city').val();
            var user_email = $('#modifier-mail').val();
            var user_birthday = $('#modifier-anniv').val();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_Gender': user_gender,
                    'User_FirstName': first_name,
                    'User_LastName': last_name,
                    'User_PhoneNumber': phone_number,
                    'User_Address': user_address,
                    'User_ZipCode': user_zipcode,
                    'User_City': user_city,
                    'User_EMail': user_email,
                    'User_BirthDay': user_birthday,
                    'action': 'event_set'
                },
                success: function (data) {
                    $(".preloader").hide();
                    if (data == 0){
                        $('#errorModifier').css('display', 'none');
                        $('.editDashboardAccount').css('display', 'none');
                        location.reload();
                    } else {
                        $('#errorModifier').css('display', 'block');
                        $('#errorModifier').html('<div><p>' + data + '</p></div>');
                    }
                },
                error: function (errorThrown) {
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#form-reset-pass').submit(function (e){
           e.preventDefault();
            $(".preloader").show();
           var phone_number = $('#input-reset-phone').val();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_PhoneNumber': phone_number,
                    'action': 'event_reset'
                },
                success:function(data) {
                    $(".preloader").hide();
                    if (data == 0){
                        $('#tagErrorMsg').css('display', 'none');
                        $('#tagSuccessMsg').css('display', 'block');
                        $('#tagSuccessMsg').html('<div><p>' + 'Votre mot de passe va vous être envoyé par SMS.' + '</p></div>');
                    } else {
                        $('#tagSuccessMsg').css('display', 'none');
                        $('#tagErrorMsg').css('display', 'block');
                        $('#tagErrorMsg').html('<div><p>' + data + '</p></div>');
                    }
                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#form-step-one').submit(function(e) {
            e.preventDefault();
            $('#section1').css('display', 'none');
            $('#section2').css('display', 'block');
            $('#step-one').removeClass('step-active');
            $('#step-one').addClass('step-done');
            $('#step-two').addClass('step-active');
        });
        $('#modifier-step-one-form').submit(function (e) {
            e.preventDefault();
            $('.modifier-user').css('display', 'none');
            $('.modifier-user-section2').css('display', 'block');
            $('#modifier-stepper-one').removeClass('step-active');
            $('#modifier-stepper-one').addClass('step-done');
            $('#modifier-stepper-two').addClass('step-active');
        });
        $('#modifier-step-two-form').submit(function (e) {
            e.preventDefault();
            $('.modifier-user-section2').css('display', 'none');
            $('.modifier-user-section3').css('display', 'block');
            $('#modifier-stepper-two').removeClass('step-active');
            $('#modifier-stepper-two').addClass('step-done');
            $('#modifier-stepper-three').addClass('step-active');
        });
        $('#form-step-two').submit(function(e) {
            e.preventDefault();
            $('#section2').css('display', 'none');
            $('#section3').css('display', 'block');
            $('#step-two').removeClass('step-active');
            $('#step-two').addClass('step-done');
            $('#step-three').addClass('step-active');
        });
        $('#modifier-stop').click(function (e) {
            e.preventDefault();
            $('.editDashboardAccount').css('display', 'none');
            $('.acc-info').css('display', 'block');
        });
        $('#link-back1').click(function (e){
            e.preventDefault();
            $('#section1').css('display', 'block');
            $('#section2').css('display', 'none');
            $('#step-two').removeClass('step-active');
            $('#step-one').removeClass('step-done');
            $('#step-one').addClass('step-active');
        });
        $('#link-back2').click(function (e){
            e.preventDefault();
            $('#section2').css('display', 'block');
            $('#section3').css('display', 'none');
            $('#step-three').removeClass('step-active');
            $('#step-two').removeClass('step-done');
            $('#step-two').addClass('step-active');
        });
        $('#link-back3').click(function (e){
            e.preventDefault();
            $('.modifier-user').css('display', 'block');
            $('.modifier-user-section2').css('display', 'none');
            $('#modifier-stepper-two').removeClass('step-active');
            $('#modifier-stepper-one').removeClass('step-done');
            $('#modifier-stepper-one').addClass('step-active');
        });
        $('#link-back4').click(function (e){
            e.preventDefault();
            $('.modifier-user-section2').css('display', 'block');
            $('.modifier-user-section3').css('display', 'none');
            $('#modifier-stepper-three').removeClass('step-active');
            $('#modifier-stepper-two').removeClass('step-done');
            $('#modifier-stepper-two').addClass('step-active');
        });
        $('#loyaltyCivilityMrs').click(function (e){
            $('#loyaltyCivilityMr').prop('checked', false);
        });
        $('#loyaltyCivilityMr').click(function (e){
            $('#loyaltyCivilityMrs').prop('checked', false);
        });
        $('#CivilityMrs').click(function (e){
            $('#CivilityMr').prop('checked', false);
        });
        $('#CivilityMr').click(function (e){
            $('#CivilityMrs').prop('checked', false);
        });
        $('#reset-pass').click(function (e){
            e.preventDefault();
            $('.login-container').css('display', 'none');
            $('.password-container').css('display', 'flex');
        });
        $('#return-login-form').click(function (e){
            e.preventDefault();
            $('.login-container').css('display', 'flex');
            $('.password-container').css('display', 'none');
        });
        $('#acc-profile').click(function (e){
            e.preventDefault();
            $('.acc-info').css('display', 'block');
            $('.acc-password').css('display', 'none');
            $('.acc-promo').css('display', 'none');
            $('.editDashboardAccount').css('display', 'none');
            $('.deleteaccount').css('display', 'none');
            $('.nav-item').removeClass('active');
            $('#acc-profile').addClass('active');
        });
        $('#acc-promotions').click(function (e){
            e.preventDefault();
            $('.acc-info').css('display', 'none');
            $('.acc-password').css('display', 'none');
            $('.deleteaccount').css('display', 'none');
            $('.editDashboardAccount').css('display', 'none');
            $('.acc-promo').css('display', 'block');
            $('.nav-item').removeClass('active');
            $('#acc-promotions').addClass('active')
        });
        $('#acc-pass').click(function (e){
            e.preventDefault();
            $('.acc-info').css('display', 'none');
            $('.acc-password').css('display', 'block');
            $('.acc-promo').css('display', 'none');
            $('.deleteaccount').css('display', 'none');
            $('.editDashboardAccount').css('display', 'none');
            $('.nav-item').removeClass('active');
            $('#acc-pass').addClass('active')
        });
        $('#edit-user').click(function (e){
            e.preventDefault();
            $(".preloader").show();
            $('.acc-info').css('display', 'none');
            $('.editDashboardAccount').css('display', 'block');
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_PhoneNumber': window.User_PhoneNumber,
                    'action': 'event_get'
                },
                success:function(data) {
                    $(".preloader").hide();
                    var dataObject = JSON.parse(data);
                    dataModified(dataObject);
                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#new-password-form').submit(function (e){
            e.preventDefault();
            $(".preloader").show();
            $('#ErrorPass').css('display', 'none');
            var phone = window.User_PhoneNumber;
            var oldPin = $('#oldPsw').val();
            var newPin = $('#newPsw').val();
            var newPinRepeat = $('#newPswCheck').val();
            if (newPin === newPinRepeat)
            {
                $.ajax({
                    url: apiajax.ajax_url,
                    type: 'POST',
                    data: {
                        'User_PhoneNumber': phone,
                        'User_NewPinCode': newPin,
                        "User_OldPinCode": oldPin,
                        'action': 'event_pin_set'
                    },
                    success:function(data) {
                        $(".preloader").hide();
                        if (data == 0){
                            console.log("True");
                            location.reload();
                        } else {
                            console.log("False");
                            $('#ErrorPass').html('<div><p>' + data + '</p></div>');
                            $('#ErrorPass').css('display', 'block');
                        }
                    },
                    error: function(errorThrown){
                        $(".preloader").hide();
                        console.log(errorThrown);
                    }
                });
                $(".preloader").hide();
            } else {
                $(".preloader").hide();
                $('#ErrorPass').html('<div><p>' + 'Les champs du nouveau mot de passe ne sont pas identiques.' + '</p></div>');
                $('#ErrorPass').css('display', 'block');
            }
        });
        $('#notification-form').submit(function (e){
            e.preventDefault();
            $(".preloader").show();
            var phone = window.User_PhoneNumber;
            if ($('#trigger-newsletter').is(':checked')){
                var email = 'True';
            } else {
                var email = 'False';
            }
            if ($('#trigger-promotion').is(':checked')){
                var sms = 'True';
            } else {
                var sms = 'False';
            }
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_PhoneNumber': phone,
                    'User_OffersByEMail': email,
                    'User_OffersBySMS': sms,
                    'action': 'event_notification'
                },
                success:function(data) {
                    $(".preloader").hide();
                    console.log(data);
                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#trigger-newsletter').click(function (e){
            if ($('#trigger-newsletter').is(':checked')){
                $("#trigger-newsletter-label").text("Vous êtes abonné à notre newsletter");
            } else {
                $("#trigger-newsletter-label").text("Souhaitez-vous vous abonner à notre newsletter ?");
            }
        });
        $('#trigger-promotion').click(function (e){
            if ($('#trigger-promotion').is(':checked')){
                $("#trigger-promotion-label").text("Vous recevez nos promotions STEEL par SMS");
            } else {
                $("#trigger-promotion-label").text("Souhaitez-vous recevoir nos promotions STEEL par SMS ?");
            }
        });
        $('#download-img').click(function (e){
            location.href =$('#user_img').attr("src");
        });
        $('#delete-user').click(function (e){
            e.preventDefault();
            $('.acc-info').css('display', 'none');
            $('.deleteaccount').css('display', 'block');
        });
        $('#deleteaccount-form').submit(function (e){
            e.preventDefault();
            $(".preloader").show();
            var phone = window.User_PhoneNumber;
            var pass = $('#password-delete').val();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'User_PhoneNumber': phone,
                    'User_PinCode': pass,
                    'action': 'event_delete'
                },
                success:function(data) {
                    $(".preloader").hide();
                    if (data == 0){
                        $('#errorDelete').css('display', 'none');
                        $(".preloader").show();
                        $.ajax({
                            url: apiajax.ajax_url,
                            type: 'POST',
                            data: {
                                'action': 'event_session'
                            },
                            success: function (data) {
                                $(".preloader").hide();
                                location.reload();
                            },
                            error: function (errorThrown) {
                                $(".preloader").hide();
                                console.log(errorThrown);
                            }
                        });
                    } else {
                        $('#errorDelete').html('<div><p>' + data + '</p></div>');
                    }
                },
                error: function(errorThrown){
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
        $('#session-kill').click(function (e){
            e.preventDefault();
            $(".preloader").show();
            $.ajax({
                url: apiajax.ajax_url,
                type: 'POST',
                data: {
                    'action': 'event_session'
                },
                success: function (data) {
                    $(".preloader").hide();
                    location.reload();
                },
                error: function (errorThrown) {
                    $(".preloader").hide();
                    console.log(errorThrown);
                }
            });
        });
    });


    $( window ).load(function() {
        $(".preloader").show();
        $.ajax({
            url: apiajax.ajax_url,
            type: 'POST',
            data: {
                'action': 'get_session'
            },
            success: function (data) {
                $(".preloader").hide();
                if (data == "true")
                {
                    $.ajax({
                        url: apiajax.ajax_url,
                        type: 'POST',
                        data: {
                            'action': 'event_get'
                        },
                        success: function (data) {
                            var dataObject = JSON.parse(data);
                            console.log(dataObject);
                            window.User_PhoneNumber = dataObject.User_PhoneNumber;
                            $('#user_tel').html($('#user_tel').text() + '<strong>' + dataObject.User_PhoneNumber + '</strong>');
                            dataDisplay(dataObject);
                            $('.login-container').css('display', 'none');
                            $('.dashboard-container').css('display', 'flex');
                        },
                        error: function (errorThrown) {
                            console.log(errorThrown);
                        }
                    });
                }
            }
        });

    });
    function dataDisplay(dataObject){
        $('#user_first_name').html( dataObject.User_FirstName +
            '&ensp;<strong>' + dataObject.User_LastName + '</strong>');
        $('#user_email').html($('#user_email').text() + '<strong>' + dataObject.User_EMail + '</strong>');
        $('#user_birthDay').html($('#user_birthDay').text() + '<strong>' + dataObject.User_BirthDay + '</strong>');
        $('#user_address').html($('#user_address').text() + dataObject.User_Address );
        $('#user_city').html($('#user_city').text() + dataObject.User_ZipCode + '&ensp;' + dataObject.User_City );
        $('#user_img').attr("src", dataObject.User_ContactlessCardImageLink);
        if (dataObject.User_OffersByEMail == "True") {
            $('#trigger-newsletter').prop('checked', true);
            $("#trigger-newsletter-label").text("Vous êtes abonné à notre newsletter");
        }
        if (dataObject.User_OffersBySMS == "True") {
            $('#trigger-promotion').prop('checked', true);
            $("#trigger-promotion-label").text("Vous recevez nos promotions STEEL par SMS");
        }
    }
    function dataModified(dataObject) {
        if (dataObject.User_Gender == 1) {
            $('#CivilityMr').prop('checked', 'true');
        } else {
            $('#CivilityMrs').prop('checked', 'true');
        }
        $('#modifier-first-name').val(dataObject.User_FirstName);
        $('#modifier-last-name').val(dataObject.User_LastName);
        $('#modifier-user-address').val(dataObject.User_Address);
        $('#modifier-user-zipcode').val(dataObject.User_ZipCode);
        $('#modifier-user-city').val(dataObject.User_City);
        $('#modifier-mail').val(dataObject.User_EMail);
        $('#modifier-anniv').val(dataObject.User_BirthDay);
    }
})( jQuery );