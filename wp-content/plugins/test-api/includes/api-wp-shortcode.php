<?php

function tag_func()
{
    ob_start();
    if (empty($_SESSION['User_PhoneNumber'])) {
        ?>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Stencil+Display:wght@100&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
        <div class="preloader"><img src="http://preloaders.net/preloaders/482/Mini%20balls.gif"></div>
        <div class="ocm-effect-wrap">
            <div class="ocm-effect-wrap-inner">
                <div id="ajax-content-wrap">
                    <div class="container-wrap">
                        <div class="container main-content">
                            <div id="app">
                                <section class="login-container">
                                    <div class="block block-customer-login">
                                        <div class="login"><h2 class="login-title">Mon compte</h2>
                                            <p data-v-72ab4c8d="" class="login-subtitle">Si vous avez un compte,
                                                connectez-vous avec votre numéro de téléphone</p>
                                            <form id="login-form" data-v-72ab4c8d="" method="post" action="#"
                                                  autocomplete="on">
                                                <input id="login" data-v-72ab4c8d="" type="text"
                                                       pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$"
                                                       placeholder="Champ téléphone*" minlength="10"
                                                       required="required">
                                                <input id="password" data-v-72ab4c8d="" type="password"
                                                       placeholder="Champ mot de passe*" minlength="6"
                                                       required="required">
                                                <div data-v-72ab4c8d="" class="form-group forgotLogin">
                                                    <div data-v-72ab4c8d="" class="form-group">
                                                        <input data-v-72ab4c8d="" type="checkbox" id="rememberCheckbox"
                                                               checked="checked">
                                                        <label data-v-72ab4c8d="" for="rememberCheckbox">Se souvenir de
                                                            moi</label>
                                                    </div>
                                                    <a id="reset-pass" data-v-72ab4c8d="" href="#" class="muted-link">Mot
                                                        de
                                                        passe oublié ?</a>
                                                </div>
                                                <input id="login-app" data-v-72ab4c8d="" type="submit"
                                                       value="Se connecter">
                                                <div id="tagErrorMsgLogin"></div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="block block-customer-register back-to-login">
                                        <div data-v-2f03da0b="" class="register">
                                            <h2 data-v-2f03da0b="" class="register-title">Créer un compte</h2>
                                            <p data-v-2f03da0b="" class="register-subtitle"> Adhérez au <strong
                                                        data-v-2f03da0b="">Programme privilèges «STEEL LOVERS»</strong>
                                            </p>
                                            <div data-v-f7f84f8a="" data-v-2f03da0b="" class="v-stepper">
                                                <div id="step-one" data-v-f7f84f8a="" class="step step-active">
                                                    <span data-v-f7f84f8a="" class="step-number">1</span>
                                                </div>
                                                <div id="step-two" data-v-f7f84f8a="" class="step">
                                                    <span data-v-f7f84f8a="" class="step-number">2</span>
                                                </div>
                                                <div id="step-three" data-v-f7f84f8a="" class="step">
                                                    <span data-v-f7f84f8a="" class="step-number">3</span>
                                                </div>
                                            </div>
                                            <section id="section1" data-v-2f03da0b="">
                                                <form id="form-step-one" data-v-2f03da0b="" method="post" action="#">
                                                    <div data-v-2f03da0b="" class="form-group">
                                                        <input data-v-2f03da0b="" id="loyaltyCivilityMr" type="radio"
                                                               name="radio" value="1" checked>
                                                        <label data-v-2f03da0b="" for="loyaltyCivilityMr"
                                                               class="input-label">Monsieur*</label>
                                                        <input data-v-2f03da0b="" id="loyaltyCivilityMrs" type="radio"
                                                               name="radio" value="2">
                                                        <label data-v-2f03da0b="" for="loyaltyCivilityMrs"
                                                               class="input-label">Madame*</label>
                                                    </div>
                                                    <div data-v-2f03da0b="" class="form-group">
                                                        <input id="first-name" data-v-2f03da0b="" type="text"
                                                               autocomplete="customer.firstName"
                                                               placeholder="Prénom*" required="required">
                                                        <input id="last-name" data-v-2f03da0b="" type="text"
                                                               autocomplete="customer.lastName"
                                                               placeholder="Nom*" required="required">
                                                    </div>
                                                    <input id="phone-number" data-v-2f03da0b="" type="tel"
                                                           autocomplete="customer.phoneNumber"
                                                           placeholder="Champ téléphone*" minlength="9" maxlength="10"
                                                           pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$"
                                                           required="required">
                                                    <div data-v-2f03da0b="" data-style="see-through"
                                                         data-alignment="right"
                                                         data-text-color="custom" class="nectar-cta">
                                                        <p data-v-2f03da0b="" class="nectar-cta-color">
                                                            <span data-v-2f03da0b="" class="text"></span>
                                                            <span data-v-2f03da0b="" class="link_wrap">
                                                            <input id="first-submit" data-v-2f03da0b="" type="submit"
                                                                   value="Suivant" class="link_text">
                                                            <span data-v-2f03da0b="" class="arrow"></span>
                                                        </span>
                                                        </p>
                                                    </div>
                                                </form>
                                            </section>
                                            <section id="section2" data-v-2f03da0b="" style="display: none;">
                                                <form id="form-step-two" data-v-2f03da0b="" method="post" action="#">
                                                    <input id="user-address" data-v-2f03da0b="" type="text"
                                                           autocomplete="customer.address"
                                                           placeholder="Adresse postale" required="required">
                                                    <div data-v-2f03da0b="" class="form-group">
                                                        <input id="user-zipcode" data-v-2f03da0b="" type="text"
                                                               autocomplete="customer.zipCode"
                                                               placeholder="Code postal" minlength="5" maxlength="5"
                                                               required="required">
                                                        <input id="user-city" data-v-2f03da0b="" type="text"
                                                               autocomplete="customer.city"
                                                               placeholder="Ville" required="required">
                                                    </div>
                                                    <div class="container-box">
                                                        <div class="box">
                                                        <span data-v-2f03da0b="" class="link_wrap">
                                                            <span id="link-back1" data-v-2f03da0b="" class="link_wrap">
                                                                <input data-v-2f03da0b="" type="button"
                                                                       value="Précédent" class="link_text">
                                                            </span>
                                                        </span>
                                                            <span data-v-2f03da0b="" class="link_wrap">
                                                            <input data-v-2f03da0b="" type="submit" value="Suivant"
                                                                   class="link_text">
                                                        </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </section>
                                            <section id="section3" data-v-2f03da0b="" style="display: none;">
                                                <form id="last-form-register" data-v-2f03da0b="" action="#">
                                                    <label data-v-2f03da0b="" for="mail" class="input-label third-step">Adresse
                                                        email</label>
                                                    <input data-v-2f03da0b="" type="email" id="mail"
                                                           autocomplete="customer.eMail"
                                                           placeholder="exemple@exemple.fr"
                                                           required="required">
                                                    <label data-v-2f03da0b="" for="anniv"
                                                           class="input-label third-step">Date
                                                        d'anniversaire </label>
                                                    <input data-v-2f03da0b="" type="date" id="anniv"
                                                           placeholder="Indiquer date de naissance : jj/mm/aaaa"
                                                           required="required">
                                                    <div data-v-2f03da0b="" class="form-group termOfService">
                                                        <input data-v-2f03da0b="" type="checkbox"
                                                               id="trigger-termOfService"
                                                               required="required">
                                                        <label data-v-2f03da0b="" for="trigger-termOfService"
                                                               class="input-label">Accepter la
                                                            <a data-v-2f03da0b="" href="/politique-de-confidentialite/"
                                                               target="_blank">politique de confidentialité</a>
                                                        </label>
                                                    </div>
                                                    <div data-v-2f03da0b="" class="form-group cta-step">
                                                        <div data-v-2f03da0b="" data-style="see-through"
                                                             data-alignment="left" data-text-color="custom"
                                                             class="nectar-cta prev">
                                                            <p data-v-2f03da0b="" class="nectar-cta-color">
                                                            <span data-v-2f03da0b="" class="link_wrap">
                                                            <span id="link-back2" data-v-2f03da0b="" class="link_wrap">
                                                                <input data-v-2f03da0b="" type="button"
                                                                       value="Précédent" class="link_text">
                                                            </span>
                                                        </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <input id="register-submit" data-v-2f03da0b="" type="submit"
                                                           value="Créer mon compte"></form>
                                            </section>
                                            <div id="errorRegister"></div>
                                        </div>
                                    </div>
                                    <div class="block block-customer-register block-customer-congrats" style="display: none">
                                            <!---->
                                            <div class="congrats" isregistered="true">
                                                <img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAMAAABG8BK2AAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAqxQTFRFAAAA////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////GB8IywAAAOR0Uk5TAAtTg6e3ua+QaSsBEE2BoLizlG4oAln2/+aLKQlevv3WIDvO/qwsCG/o9wee/I0KQdzgPLvdMgWSSccZxPpV+dfA1fJ3Id7lutLqLmPFDgM+ju+AJeNXFzPTI/PxXIT1eR/iwantNSbr+3sEpiQnmOQ5nImKh2yl9DHwlhbKUZvLjEMGfbYtS0iyz9SkR6hfdX7seBJ0HltwTtlz+L8bkRgRUsJgdkY6zJVio61yNsjpcT+Pqg1EDLzuZ2saN09FzV3bHLBlQFDYFd+F0Jd/L2i1nUrDVBS95zDGD1YiPdqTtNGxA2MfLgAABTdJREFUeJylWPtDVFUQHjLABIFS5GIIaRTCtomGII91xRdBAaICKpQiWG2olZqAD+SRCWpaBoqChpFIVmgmgVmQSZo9rOz9tvpHuntmzt1z7gNYml/2zMw3H+fOnTN3DgCi+Nw25nZfP/+xd4wLAHMJHB8UHBJy510TJloAIHSSf5jCJXzy3UZExJTIKA1xz5ipZizT7lUkib7vfhkQMz1WRsQFRehJbA8oBrE/OENAxM80ImY9JLP4JBgxqsxO5ICkOdFmgLBkKXEppizqvqcgIDXNAuCYK9D4a2bnvPSE+QsE3EK3f9FiwZLxcHpmlifgEY3lUW7LnpTj1nOX5C3VcMsAludrWkpBIQtZMZm/1ZU2YllVhIbix4THfHw1j1xTspYvS4WUloSQMY8MZaiue0LK+pNaATxFv66nQ0VA+Xo02zcwdSOWVNQ0fQ08I2fz2ed0gNBIdGxi2iZUNoNBtogs+asM/hJ8F45UdZ2E2YyNMdLA88JejCwAa9C3VV0m43KhCQqggrNUVpm5c7YxZ7q63I6Z2WFKY9tJNNWmbtiFwYEANRqhmUzFR661cNOj1EG9ky1esMBBskP17k6y8EbYKSMvIt8eKxpoaNy7xSz9KPtY9H54CWlMW9AI5ACLjoSDSGNoQCOUOSx6HrzMflePkgVeYeGH4FX265SdTdXNyWb7O3ykZYnc7INY+FKiUaQkHnUfxmMrDCytbar9+AnR9BrR4EMp7YJrYhwzZZzUsbyO0A7xnAcz0zp4A30bBdcuqtxTOhreOeoEmz+luBNdpwVXF8HLZJYkh2I8fh30wseha7rg8iP4m7rd8PZ1xmMqx8b5FpRjIt4W4O8o5pVNx71NKNVuNLUCnGWLLAFevpMqXCft55j9XcHUjDTn+StTxE/2jPfClcXNoXoaSL2wLSpTahmNLNbVA/C+6WvxMXAwCZCLshe7aJ/7ES6y5UyrUWQo+QC3cMm9piEgebgYE8lkkfYP3euPkCbNe5Z+jPRHjapzwFuWgL0Y2IkqtZyPc72kuYxx8ymruZ+g3uIdSxONFQXcQNspuiKAensMcUmXBoO7PSq9mn1afeViY1YGNUhPmUs53inVQM+n7k07r3L9PB2Zfg8knkzXuOECU/O3H8VpBtqPVNCgkkII22eo+4l/CsOUjOuo+ngGvbXpZRWln2uq2nZRalF1iI0KrhSjNQEfNMcz/uqlDwOoZJQv5PTxuW0ZqrutWCpx1FtEQ93Ket1roMAoLMLrXeYsHcsxMeR2Gdr+iUPoycLGH3rwSwNHtC8v9K/IckPPArCH0lrDR/Ovb2TbPRyOjlNa1/uGbJHGlgSwmZyNnkMRONBau78v+9ubrXVCA/qO2GPbTVig/Hvi0bdynSRWIsxeZ+4v5PNv81Ash8MJZXkEq6hSowusEAAn6eAoN60xP1CaHeOtEDl8GEzTV4wo9EVX2jrN/T78JnPsxyFYAMYSLOyqmTfmJ3L/PMx0FnCAgEWnjU6bLzmLfxmaBaCef8GLDPuJ4HeWuPjhWFRwOn+uBtnRyx3OBvNIHfxXDpe+s9pF0tVvFSnLbzU84LLHuIMPSfbfR8ai/uGzFBKt1fMf/PrsmjtUpCw52kU3D9t6Fb/JO68NEypJbx/nWe++kQ7wu3CYl1962yDnKW2CMy5eL396x6K2jWDO89ctvjqXOHycQXgb02T2hlGwAEyQPzNdhaNiAagOE1j+tpjjRiDdnv/Y3PJ2bBGl5B8q3a3/g0SVXnYpuOj1JKaXgJZZC/41u8pL8h95amndqnWPKQAAAABJRU5ErkJggg=="
                                                        alt="Programme de fidélité STEEL" width="70" height="70"
                                                        class="congrats-img alignnone size-full">
                                                <h2 class="congrats-title">BRAVO !</h2>
                                                <p class="congrats-subtitle"> Vous faites désormais partie de la tribu des <br>
                                                    <strong>STEEL LOVERS</strong>
                                                    <br> A vous les privilèges toute l’année !
                                                </p>
                                                <p class="congrats-text"> Vous allez recevoir
                                                    un mail/sms avec vos codes d’accès.<br>
                                                    Nous avons hâte de vous accueillir avec grand plaisir à
                                                    STEEL
                                                </p>
                                                <input id="logged-app" type="submit" value="Tout le détail de vos privilèges">
                                                <div data-style="see-through" data-alignment="center"
                                                     data-text-color="custom" class="nectar-cta prev cancel">
                                                    <p class="nectar-cta-color">
                                                        <span class="text"></span>
                                                        <span class="link_wrap">
                                                            <input id="return-register-form" type="submit" value="Revenir sur la connexion" class="link_text">
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                </section>
                                <section class="password-container" style="display: none">
                                    <div data-v-bbde4858="" class="block-outer-pass">
                                        <div data-v-bbde4858="" class="block-password">
                                            <h2 class="send-password">Mot de passe oublié</h2>
                                            <p class="send-password-p">Renseignez votre numéro de téléphone<br
                                                        data-v-bbde4858="">pour récupérer votre mot de passe.</p>
                                            <form id="form-reset-pass" data-v-bbde4858="" method="post" action="#">
                                                <input id="input-reset-phone" data-v-bbde4858="" type="text"
                                                       autocomplete="on"
                                                       pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$"
                                                       placeholder="Numéro de téléphone*" minlength="10"
                                                       required="required">
                                                <input id="password-reset-phone" data-v-bbde4858="" type="submit"
                                                       value="Recevoir le mot de passe">
                                                <div id="tagErrorMsg"></div>
                                                <div id="tagSuccessMsg"></div>
                                            </form><!---->
                                        </div>
                                        <div data-v-bbde4858="" data-style="see-through" data-alignment="center"
                                             data-text-color="custom" class="nectar-cta prev cancel">
                                            <p data-v-bbde4858="" class="nectar-cta-color">
                                                <span data-v-bbde4858="" class="text"></span>
                                                <span data-v-bbde4858="" class="link_wrap">
                                                <input id="return-login-form" data-v-bbde4858="" type="submit"
                                                       value="Retour à l'accueil" class="link_text">
                                            </span>
                                            </p>
                                        </div>
                                    </div>
                                </section>
                                <section class="dashboard-container" style="display: none">
                                    <div class="dashboard" ismyaccountvisible="true">
                                        <section class="dashboard-nav">
                                            <ul class="nav-items">
                                                <li id="acc-profile" class="nav-item active"><a href="#">Mon compte</a>
                                                </li>
                                                <li id="acc-promotions" class="nav-item"><a href="#">Mes abonnements</a>
                                                </li>
                                                <li id="acc-pass" class="nav-item"><a href="#">Mon mot de passe</a></li>
                                            </ul>
                                            <button id="session-kill" class="nectar-button see-through-2">Déconnexion
                                            </button>
                                        </section>
                                        <section class="dashboard-wrapper">
                                            <div class="acc-info">
                                                <a href="#customer-card" class="anchor-card">
                                                    <img src="https://prodapi.mobiwoom.com/?Short=RENDSV82MzI0NF82OTY3MQ__">
                                                    <small>Accéder au téléchargement de ma carte</small>
                                                </a>
                                                <h2 class="dashboard-wrapper-title">Informations du compte</h2>
                                                <div class="dashboard-wrapper-item">
                                                    <div class="dashboard-wrapper-data">
                                                        <p class="dashboard-wrapper-data-title">Informations de
                                                            contact</p>
                                                        <div class="dashboard-wrapper-data-head">
                                                            <p id="user_first_name" class="dashboard-wrapper-data-name">
                                                                <span id="user_last_name" class="family-name"></span>
                                                            </p>
                                                        </div>
                                                        <p id="user_tel">tél: </p>
                                                        <p id="user_email">e-mail: </p>
                                                        <p id="user_birthDay"><span>né</span> le: </p>
                                                        <p id="user_address"></p>
                                                        <p id="user_city" class="zipcode"></p>
                                                        <input id="edit-user" data-v-72ab4c8d="" type="submit"
                                                               value="Modifier vos informations">
                                                        <div>
                                                            <input id="delete-user" data-v-bbde4858="" type="submit"
                                                                   value="Supprimer votre compte" class="link_text">
                                                        </div>

                                                    </div>
                                                    <div class="dashboard-wrapper-data">
                                                        <p class="dashboard-wrapper-data-title">Carte Privilège</p>
                                                        <div id="customer-card">
                                                            <img id="user_img" src="" class="customer-card">
                                                        </div>
                                                        <input id="download-img" data-v-72ab4c8d="" type="submit"
                                                               value="Télécharger ma carte">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="acc-promo" style="display: none">
                                                <div class="subscription">
                                                    <h2 class="subscription-title">Modifier mes informations</h2>
                                                    <p class="subscription-subtitle">État de vos abonnements</p>
                                                    <section>
                                                        <form id="notification-form" method="post" action="#">
                                                            <div class="form-group newsletter">
                                                                <input type="checkbox" id="trigger-newsletter">
                                                                <label id="trigger-newsletter-label"
                                                                       for="trigger-newsletter" class="input-label">
                                                                    Souhaitez-vous vous abonner à notre newsletter
                                                                    ?</label>
                                                            </div>
                                                            <div class="form-group promotion">
                                                                <input type="checkbox" id="trigger-promotion">
                                                                <label id="trigger-promotion-label"
                                                                       for="trigger-promotion"
                                                                       class="input-label">
                                                                    Souhaitez-vous recevoir nos promotions STEEL par SMS
                                                                    ?</label>
                                                            </div>
                                                            <input id="submit-promo" type="submit"
                                                                   value="Mettre à jour mes abonnements"
                                                                   class="subscription-submit">
                                                        </form>
                                                        <div class="dashboard-messages-wrapper"></div>
                                                    </section>
                                                </div>
                                            </div>
                                            <div class="acc-password" style="display: none">
                                                <h2 class="password-title">Modifier mon mot de passe</h2>
                                                <p class="password-subtitle">Changement de mot de passe</p>
                                                <form id="new-password-form" method="post" action="#"
                                                      class="password-form">
                                                    <div class="form-group">
                                                        <label class="password-form-label" for="oldPsw">Saissisez votre
                                                            mot
                                                            de passe actuel</label>
                                                        <input type="password" id="oldPsw"
                                                               placeholder="Ancien mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <div class="form-group new-password">
                                                        <label class="password-form-label" for="newPsw">Saissisez votre
                                                            nouveau mot de passe ( format : 6 chiffres) </label>
                                                        <input type="password" id="newPsw"
                                                               placeholder="Nouveau mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <div id="new-pass-inputs" class="form-group">
                                                        <label class="password-form-label" for="newPswCheck">Répétez
                                                            votre
                                                            nouveau mot de passe</label>
                                                        <input type="password" id="newPswCheck"
                                                               placeholder="Confirmation du nouveau mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <input id="submit-pass" type="submit"
                                                           value="Changer mon mot de passe"
                                                           class="password-submit">
                                                    <div id="ErrorPass"></div>
                                                </form>
                                                <div data-style="see-through" data-alignment="left"
                                                     data-text-color="custom"
                                                     class="nectar-cta prev cancel">
                                                    <p class="nectar-cta-color"><span class="text"></span></p>
                                                </div>
                                            </div>
                                            <div class="editDashboardAccount" style="display: none">
                                                <h2 class="editDashboardAccount-title">Modifier mon compte</h2>
                                                <p class="editDashboardAccount-subtitle">Renseigner les bonnes
                                                    informations dans chaque champs</p>
                                                <div class="v-stepper">
                                                    <div id="modifier-stepper-one" class="step step-active">
                                                        <span class="step-number">1</span>
                                                    </div>
                                                    <div id="modifier-stepper-two" class="step">
                                                        <span class="step-number">2</span>
                                                    </div>
                                                    <div id="modifier-stepper-three" class="step">
                                                        <span class="step-number">3</span>
                                                    </div>
                                                </div>
                                                <section class="modifier-user">
                                                    <form id="modifier-step-one-form" method="post" action="#">
                                                        <div class="form-group">
                                                            <input id="CivilityMr" name="radio-modifier" type="radio"
                                                                   value="1">
                                                            <label for="loyaltyCivilityMr"
                                                                   class="input-label">Monsieur</label>
                                                            <input id="CivilityMrs" name="radio-modifier" type="radio"
                                                                   value="2">
                                                            <label for="loyaltyCivilityMrs"
                                                                   class="input-label">Madame</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <input id="modifier-first-name" type="text"
                                                                   autocomplete="customer.firstName"
                                                                   placeholder="Prénom" required="required">
                                                            <input id="modifier-last-name" type="text"
                                                                   autocomplete="customer.lastName"
                                                                   placeholder="Nom" required="required">
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom"
                                                                 class="nectar-cta prev cancel">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input id="modifier-stop" type="submit"
                                                                           value="Annuler la modification"
                                                                           class="link_text">
                                                                </span>
                                                                </p>
                                                            </div>
                                                            <div data-style="see-through" data-alignment="right"
                                                                 data-text-color="custom" class="nectar-cta">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input id="modifier-step-one" type="submit"
                                                                           value="Suivant" class="link_text">
                                                                </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </section>
                                                <section class="modifier-user-section2" style="display: none;">
                                                    <form id="modifier-step-two-form" method="post" action="#">
                                                        <input id="modifier-user-address" type="text"
                                                               autocomplete="customer.address" placeholder="Adresse">
                                                        <div class="form-group">
                                                            <input id="modifier-user-zipcode" type="text"
                                                                   autocomplete="customer.zipCode"
                                                                   placeholder="Code postal" minlength="5"
                                                                   required="required">
                                                            <input id="modifier-user-city" type="text"
                                                                   autocomplete="customer.city" placeholder="Ville"
                                                                   required="required">
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom" class="nectar-cta prev">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <a href="#" class="link_text">Précédent
                                                                        <span class="arrow"></span>
                                                                    </a>
                                                                </span>
                                                                </p>
                                                            </div>
                                                            <div data-style="see-through" data-alignment="right"
                                                                 data-text-color="custom" class="nectar-cta">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input type="submit" value="Suivant"
                                                                           class="link_text">
                                                                    <span class="arrow"></span>
                                                                </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </section>
                                                <section class="modifier-user-section3" style="display: none;">
                                                    <form id="modifier-last-form" action="#">
                                                        <input id="modifier-mail" type="email"
                                                               autocomplete="customer.eMail"
                                                               placeholder="adresse email" minlength="9"
                                                               required="required">
                                                        <input id="modifier-anniv" type="date"
                                                               placeholder="Date de naissance" required="required">
                                                        <div class="form-group termOfService">
                                                            <input type="checkbox" id="trigger-termOfService"
                                                                   required="required">
                                                            <label for="trigger-termOfService" class="input-label">Accepter
                                                                la
                                                                <a href="/politique-de-confidentialite/"
                                                                   target="_blank">politique de confidentialité</a>
                                                            </label>
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom" class="nectar-cta prev">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <a href="#" class="link_text">Précédent
                                                                        <span class="arrow"></span>
                                                                    </a>
                                                                </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <input type="submit" value="Modifier mon compte">
                                                    </form>
                                                </section>
                                                <div id="errorModifier"></div>
                                            </div>
                                            <div class="deleteaccount" style="display: none">
                                                <h2 class="deleteaccount-title">Supprimer mon compte</h2>
                                                <p class="deleteaccount-subtitle">Toute suppression de compte est
                                                    définitive.</p>
                                                <form id="deleteaccount-form" method="post" action="#"
                                                      class="deleteaccount-form">
                                                    <div class="form-group new-password">
                                                        <label class="deleteaccount-label" for="newPsw">Veuillez saisir
                                                            votre mot de passe</label>
                                                        <input type="password" id="password-delete"
                                                               placeholder="Mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <input id="deleteaccount-sbmt" type="submit"
                                                           value="Supprimer mon compte" class="deleteaccount-submit">
                                                    <div id="errorDelete"></div>
                                                </form>
                                                <div data-style="see-through" data-alignment="left"
                                                     data-text-color="custom" class="nectar-cta prev cancel">
                                                    <p class="nectar-cta-color">
                                                        <span class="text"></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } elseif (!empty($_SESSION['User_PhoneNumber'])) {
        ?>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Stencil+Display:wght@100&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
        <div class="preloader"><img src="http://preloaders.net/preloaders/482/Mini%20balls.gif"></div>
        <div class="ocm-effect-wrap">
            <div class="ocm-effect-wrap-inner">
                <div id="ajax-content-wrap">
                    <div class="container-wrap">
                        <div class="container main-content">
                            <div id="app">
                                <section class="dashboard-container">
                                    <div class="dashboard" ismyaccountvisible="true">
                                        <section class="dashboard-nav">
                                            <ul class="nav-items">
                                                <li id="acc-profile" class="nav-item active"><a href="#">Mon compte</a>
                                                </li>
                                                <li id="acc-promotions" class="nav-item"><a href="#">Mes abonnements</a>
                                                </li>
                                                <li id="acc-pass" class="nav-item"><a href="#">Mon mot de passe</a></li>
                                            </ul>
                                            <button id="session-kill" class="nectar-button see-through-2">Déconnexion
                                            </button>
                                        </section>
                                        <section class="dashboard-wrapper">
                                            <div class="acc-info">
                                                <a href="#customer-card" class="anchor-card">
                                                    <small>Accéder au téléchargement de ma carte</small>
                                                </a>
                                                <h2 class="dashboard-wrapper-title">Informations du compte</h2>
                                                <div class="dashboard-wrapper-item">
                                                    <div class="dashboard-wrapper-data">
                                                        <p class="dashboard-wrapper-data-title">Informations de
                                                            contact</p>
                                                        <div class="dashboard-wrapper-data-head">
                                                            <p id="user_first_name" class="dashboard-wrapper-data-name">
                                                                <span id="user_last_name" class="family-name"></span>
                                                            </p>
                                                        </div>
                                                        <p>tél: <span id="user_tel"></span></p>
                                                        <p id="user_email">e-mail: </p>
                                                        <p id="user_birthDay"><span>né</span> le: </p>
                                                        <p id="user_address"></p>
                                                        <p id="user_city" class="zipcode"></p>
                                                        <input id="edit-user" data-v-72ab4c8d="" type="submit"
                                                               value="Modifier vos informations">
                                                        <div>
                                                            <input id="delete-user" data-v-bbde4858="" type="submit"
                                                                   value="Supprimer votre compte" class="link_text">
                                                        </div>

                                                    </div>
                                                    <div class="dashboard-wrapper-data">
                                                        <p class="dashboard-wrapper-data-title">Carte Privilège</p>
                                                        <div id="customer-card">
                                                            <img id="user_img" src="" class="customer-card">
                                                        </div>
                                                        <input id="download-img" data-v-72ab4c8d="" type="submit"
                                                               value="Télécharger ma carte">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="acc-promo" style="display: none">
                                                <div class="subscription">
                                                    <h2 class="subscription-title">Modifier mes informations</h2>
                                                    <p class="subscription-subtitle">État de vos abonnements</p>
                                                    <section>
                                                        <form id="notification-form" method="post" action="#">
                                                            <div class="form-group newsletter">
                                                                <input type="checkbox" id="trigger-newsletter">
                                                                <label id="trigger-newsletter-label"
                                                                       for="trigger-newsletter" class="input-label">
                                                                    Souhaitez-vous vous abonner à notre newsletter
                                                                    ?</label>
                                                            </div>
                                                            <div class="form-group promotion">
                                                                <input type="checkbox" id="trigger-promotion">
                                                                <label id="trigger-promotion-label"
                                                                       for="trigger-promotion"
                                                                       class="input-label">
                                                                    Souhaitez-vous recevoir nos promotions STEEL par SMS
                                                                    ?</label>
                                                            </div>
                                                            <input id="submit-promo" type="submit"
                                                                   value="Mettre à jour mes abonnements"
                                                                   class="subscription-submit">
                                                        </form>
                                                        <div class="dashboard-messages-wrapper"></div>
                                                    </section>
                                                </div>
                                            </div>
                                            <div class="acc-password" style="display: none">
                                                <h2 class="password-title">Modifier mon mot de passe</h2>
                                                <p class="password-subtitle">Changement de mot de passe</p>
                                                <form id="new-password-form" method="post" action="#"
                                                      class="password-form">
                                                    <div class="form-group">
                                                        <label class="password-form-label" for="oldPsw">Saissisez votre
                                                            mot
                                                            de passe actuel</label>
                                                        <input type="password" id="oldPsw"
                                                               placeholder="Ancien mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <div class="form-group new-password">
                                                        <label class="password-form-label" for="newPsw">Saissisez votre
                                                            nouveau mot de passe ( format : 6 chiffres) </label>
                                                        <input type="password" id="newPsw"
                                                               placeholder="Nouveau mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <div id="new-pass-inputs" class="form-group">
                                                        <label class="password-form-label" for="newPswCheck">Répétez
                                                            votre
                                                            nouveau mot de passe</label>
                                                        <input type="password" id="newPswCheck"
                                                               placeholder="Confirmation du nouveau mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <input id="submit-pass" type="submit"
                                                           value="Changer mon mot de passe"
                                                           class="password-submit">
                                                    <div id="ErrorPass"></div>
                                                </form>
                                                <div data-style="see-through" data-alignment="left"
                                                     data-text-color="custom"
                                                     class="nectar-cta prev cancel">
                                                    <p class="nectar-cta-color"><span class="text"></span></p>
                                                </div>
                                            </div>
                                            <div class="editDashboardAccount" style="display: none">
                                                <h2 class="editDashboardAccount-title">Modifier mon compte</h2>
                                                <p class="editDashboardAccount-subtitle">Renseigner les bonnes
                                                    informations dans chaque champs</p>
                                                <div class="v-stepper">
                                                    <div id="modifier-stepper-one" class="step step-active">
                                                        <span class="step-number">1</span>
                                                    </div>
                                                    <div id="modifier-stepper-two" class="step">
                                                        <span class="step-number">2</span>
                                                    </div>
                                                    <div id="modifier-stepper-three" class="step">
                                                        <span class="step-number">3</span>
                                                    </div>
                                                </div>
                                                <section class="modifier-user">
                                                    <form id="modifier-step-one-form" method="post" action="#">
                                                        <div class="form-group">
                                                            <input id="CivilityMr" name="radio-modifier" type="radio"
                                                                   value="1">
                                                            <label for="loyaltyCivilityMr"
                                                                   class="input-label">Monsieur</label>
                                                            <input id="CivilityMrs" name="radio-modifier" type="radio"
                                                                   value="2">
                                                            <label for="loyaltyCivilityMrs"
                                                                   class="input-label">Madame</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <input id="modifier-first-name" type="text"
                                                                   autocomplete="customer.firstName"
                                                                   placeholder="Prénom" required="required">
                                                            <input id="modifier-last-name" type="text"
                                                                   autocomplete="customer.lastName"
                                                                   placeholder="Nom" required="required">
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom"
                                                                 class="nectar-cta prev cancel">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input id="modifier-stop" type="submit"
                                                                           value="Annuler la modification"
                                                                           class="link_text">
                                                                </span>
                                                                </p>
                                                            </div>
                                                            <div data-style="see-through" data-alignment="right"
                                                                 data-text-color="custom" class="nectar-cta">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input id="modifier-step-one" type="submit"
                                                                           value="Suivant" class="link_text">
                                                                </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </section>
                                                <section class="modifier-user-section2" style="display: none;">
                                                    <form id="modifier-step-two-form" method="post" action="#">
                                                        <input id="modifier-user-address" type="text"
                                                               autocomplete="customer.address" placeholder="Adresse">
                                                        <div class="form-group">
                                                            <input id="modifier-user-zipcode" type="text"
                                                                   autocomplete="customer.zipCode"
                                                                   placeholder="Code postal" minlength="5"
                                                                   required="required">
                                                            <input id="modifier-user-city" type="text"
                                                                   autocomplete="customer.city" placeholder="Ville"
                                                                   required="required">
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom" class="nectar-cta prev">
                                                                <p class="nectar-cta-color">
                                                                    <span data-v-2f03da0b="" class="link_wrap">
                                                                        <span data-v-2f03da0b="" class="link_wrap">
                                                                        <input id="link-back3" data-v-2f03da0b=""
                                                                               type="button"
                                                                               value="Précédent" class="link_text">
                                                                        </span>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div data-style="see-through" data-alignment="right"
                                                                 data-text-color="custom" class="nectar-cta">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span class="link_wrap">
                                                                    <input type="submit" value="Suivant"
                                                                           class="link_text">
                                                                    <span class="arrow"></span>
                                                                </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </section>
                                                <section class="modifier-user-section3" style="display: none;">
                                                    <form id="modifier-last-form" action="#">
                                                        <input id="modifier-mail" type="email"
                                                               autocomplete="customer.eMail"
                                                               placeholder="adresse email" minlength="9"
                                                               required="required">
                                                        <input id="modifier-anniv" type="date"
                                                               placeholder="Date de naissance" required="required">
                                                        <div class="form-group termOfService">
                                                            <input type="checkbox" id="trigger-termOfService"
                                                                   required="required">
                                                            <label for="trigger-termOfService" class="input-label">Accepter
                                                                la
                                                                <a href="/politique-de-confidentialite/"
                                                                   target="_blank">politique de confidentialité</a>
                                                            </label>
                                                        </div>
                                                        <div class="form-group cta-step">
                                                            <div data-style="see-through" data-alignment="left"
                                                                 data-text-color="custom" class="nectar-cta prev">
                                                                <p class="nectar-cta-color">
                                                                    <span class="text"></span>
                                                                    <span id="link-back4" data-v-2f03da0b=""
                                                                          class="link_wrap">
                                                                        <input data-v-2f03da0b="" type="button"
                                                                               value="Précédent" class="link_text">
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <input id="submit-modifier" type="submit"
                                                               value="Modifier mon compte">
                                                    </form>
                                                </section>
                                                <div id="errorModifier"></div>
                                            </div>
                                            <div class="deleteaccount" style="display: none">
                                                <h2 class="deleteaccount-title">Supprimer mon compte</h2>
                                                <p class="deleteaccount-subtitle">Toute suppression de compte est
                                                    définitive.</p>
                                                <form id="deleteaccount-form" method="post" action="#"
                                                      class="deleteaccount-form">
                                                    <div class="form-group new-password">
                                                        <label class="deleteaccount-label" for="newPsw">Veuillez saisir
                                                            votre mot de passe</label>
                                                        <input type="password" id="password-delete"
                                                               placeholder="Mot de passe"
                                                               minlength="6" maxlength="6" required="required">
                                                    </div>
                                                    <input id="deleteaccount-sbmt" type="submit"
                                                           value="Supprimer mon compte" class="deleteaccount-submit">
                                                    <div id="errorDelete"></div>
                                                </form>
                                                <div data-style="see-through" data-alignment="left"
                                                     data-text-color="custom" class="nectar-cta prev cancel">
                                                    <p class="nectar-cta-color">
                                                        <span class="text"></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    $out1 = ob_get_contents();
    ob_end_clean();

    return $out1;
}