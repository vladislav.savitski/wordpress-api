<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class Api_Wp {

    protected $loader;
    protected $plugin_name;
    protected $version;

    public function __construct() {

        $this->plugin_name = 'api-wp';
        $this->version = '1.1.0';
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    private function load_dependencies() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-api-wp-loader.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/api-wp-shortcode.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-api-wp-admin.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-api-wp-public.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-api-request.php';
        $this->loader = new Api_Wp_Loader();
        add_shortcode( 'apitag', 'tag_func' );
    }

    private function define_admin_hooks() {
        $plugin_admin = new Api_Wp_Admin( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_options_page' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'register_setting' );
    }

    private function define_public_hooks() {
        $plugin_public = new Api_Wp_Public( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
        $this->loader->add_action( 'wp_ajax_event_login', $plugin_public , 'event_login');
        $this->loader->add_action( 'wp_ajax_nopriv_event_login', $plugin_public , 'event_login');
        $this->loader->add_action( 'wp_ajax_event_set', $plugin_public , 'event_set');
        $this->loader->add_action( 'wp_ajax_nopriv_event_set', $plugin_public , 'event_set');
        $this->loader->add_action( 'wp_ajax_event_reset', $plugin_public , 'event_reset');
        $this->loader->add_action( 'wp_ajax_nopriv_event_reset', $plugin_public , 'event_reset');
        $this->loader->add_action( 'wp_ajax_event_get', $plugin_public , 'event_get');
        $this->loader->add_action( 'wp_ajax_nopriv_event_get', $plugin_public , 'event_get');
        $this->loader->add_action( 'wp_ajax_event_pin_set', $plugin_public , 'event_pin_set');
        $this->loader->add_action( 'wp_ajax_nopriv_event_pin_set', $plugin_public , 'event_pin_set');
        $this->loader->add_action( 'wp_ajax_event_notification', $plugin_public , 'event_notification');
        $this->loader->add_action( 'wp_ajax_nopriv_event_notification', $plugin_public , 'event_notification');
        $this->loader->add_action( 'wp_ajax_event_delete', $plugin_public , 'event_delete');
        $this->loader->add_action( 'wp_ajax_nopriv_event_delete', $plugin_public , 'event_delete');
        $this->loader->add_action('wp_ajax_event_session', $plugin_public, 'event_session');
        $this->loader->add_action( 'wp_ajax_nopriv_event_session', $plugin_public , 'event_session');
        $this->loader->add_action('wp_ajax_get_session', $plugin_public, 'get_session');
        $this->loader->add_action( 'wp_ajax_nopriv_get_session', $plugin_public , 'get_session');
        $this->loader->add_action( 'wp_ajax_event_register', $plugin_public , 'event_register');
        $this->loader->add_action( 'wp_ajax_nopriv_event_register', $plugin_public , 'event_register');
    }

    public function run() {
        $this->loader->run();
    }


    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }

}
