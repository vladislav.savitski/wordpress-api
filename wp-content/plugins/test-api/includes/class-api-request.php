<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Api_Wp_Request
{

    private $option_name = 'api_opt';

    public function __construct() {
        $this->apikey = get_option( $this->option_name . '_apikey' );
        $this->partner_key = get_option( $this->option_name . '_partner_key' );
        $this->url = get_option( $this->option_name . '_url' );
        $this->url_sys = get_option( $this->option_name . '_url_sys' );
    }

    public function sendData($data) {
        $headers = array(
            'method' => 'POST',
            'Content-Type' => 'application/json; charset=utf-8',
            'timeout' => 45,
            'blocking' => true,
            'httpversion' => '1.0',
            'body' => $data,
            'cookies' => array(),
        );
        $json = json_decode($data);
        $method = $json->Data->Api;

        if($method == 'UserLogin' || $method == 'UserPinCodeGet' || $method == 'UserPinCodeSet') {
            $response = wp_remote_post($this->url, $headers);
        } else {
            $response = wp_remote_post($this->url_sys, $headers);
        }

        $response_body = wp_remote_retrieve_body( $response );

        $response_body = preg_replace("/\\\\/",'', $response_body);
        $response_json = json_decode($response_body);
        if ($response_json->Response->ErrorNumber != 0) {
            return $response_json->Response->Message;
        }

        return $response_json->Response->ErrorNumber;
    }

    public function dataPhoneJson($phone, $method) {
        $data = [
            'Api' => $method,
            'Partner_Token' => $this->partner_key,
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'Common_ApplicationToken' => $this->apikey
        ];

        return json_encode(['Data'=>$data]);
    }

    public function dataPhonePinJson($phone, $pin, $method) {
        $data = [
            'Api' => $method,
            'Partner_Token' => $this->partner_key,
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'User_PinCode' => $pin,
            'Common_ApplicationToken' => $this->apikey
        ];

        return json_encode(['Data'=>$data]);
    }

    public function set($phone, $gender, $firstName, $lastname, $address, $city, $zipcode, $email, $birthday) {
        $data = [
            'Api' => 'UserSet',
            'Partner_Token' => $this->partner_key,
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'User_Gender' => $gender,
            'User_FirstName' => $firstName,
            'User_LastName' => $lastname,
            'User_Address' => $address,
            'User_Country' => 'FR',
            'User_ZipCode' => $zipcode,
            'User_City' => $city,
            'User_Language' => "FR",
            'User_EMail' => $email,
            'User_BirthDay' => $birthday,
            'Common_ApplicationToken' => $this->apikey
        ];

        return $this->sendData(json_encode(['Data'=>$data]));
    }

    public function register($phone, $gender, $firstName, $lastname, $address, $city, $zipcode, $email, $birthday) {
        $data = [
            'Api' => 'UserSet',
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'User_Gender' => $gender,
            'User_FirstName' => $firstName,
            'User_LastName' => $lastname,
            'User_Address' => $address,
            'User_Country' => 'FR',
            'User_ZipCode' => $zipcode,
            'User_City' => $city,
            'User_Language' => "FR",
            'User_EMail' => $email,
            'User_BirthDay' => $birthday,
            'User_OffersByEMail' => 'True',
            'User_OffersBySMS' => 'True',
            'User_TermsOfService' => 'True',
            'Partner_Token' => $this->partner_key,
            'Common_ApplicationToken' => $this->apikey
        ];

        return $this->sendData(json_encode(['Data'=>$data]));
    }

    public function reset($phone) {
        $data = $this->dataPhoneJson($phone, 'UserPinCodeGet');

        return $this->sendData($data);
    }

    public function get($phone) {
        $data = $this->dataPhoneJson($phone, 'UserGet');
        $headers = array(
            'method' => 'POST',
            'timeout' => 45,
            'blocking' => true,
            'httpversion' => '1.0',
            'body' => $data,
            'cookies' => array(),
        );
        $response = wp_remote_post( $this->url_sys, $headers );
        $response_body = wp_remote_retrieve_body( $response );

        $response_body = preg_replace("/\\\\/",'', $response_body);
        $response_json = json_decode($response_body);
        if ($response_json->Response->ErrorNumber != 0) {
            return $response_json->Response->Message;
        }

        return $response_json->Response->Data;
    }

    public function pin_set($phone, $oldPin, $newPin){
        $data = [
            'Api' => 'UserPinCodeSet',
            'Partner_Token' => $this->partner_key,
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'User_NewPinCode' => $newPin,
            'User_OldPinCode' => $oldPin,
            'Common_ApplicationToken' => $this->apikey,
        ];

        return $this->sendData(json_encode(['Data'=>$data]));
    }

    public function notification($phone, $email, $sms){
        $data = [
            'Api' => 'UserSet',
            'Partner_Token' => $this->partner_key,
            'User_PhoneNumber' => $phone,
            'User_PhoneCountry' => 'FR',
            'User_OffersByEMail' => $email,
            'User_OffersBySMS' => $sms,
            'Common_ApplicationToken' => $this->apikey
        ];

        return $this->sendData(json_encode(['Data'=>$data]));
    }

    public function delete($phone, $password) {
        $data = $this->dataPhonePinJson($phone, $password, 'UserDelete');

        return $this->sendData($data);
    }

    public function login($phone, $password) {
        $data = $this->dataPhonePinJson($phone, $password, 'UserLogin');

        return $this->sendData($data);
    }
}
